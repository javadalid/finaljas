package deloitte.academy.lesson01.run;

import java.util.InputMismatchException;
import java.util.Scanner;

import deloitte.academy.lesson01.machine.Maquina;

/**
 * Clase EjecutarMaquina (main). Hace uso de todos los m�todos de la m�quina
 * expendedora. Es la clase principal que da la funcionalidad de la m�quina
 * expendedora.
 * 
 * @author Javier Adalid Schmid
 * @version 1.0 La primer versi�n del programa.
 * @since 10 de marzo de 2020.
 *
 */
public class EjecutarMaquina {

	/**
	 * M�todo main. Ejecuta el men� que permite acceder a la funcionalidad
	 *
	 * @param args los argumentos del main.
	 * @exception InputMismatchException Al momento de ingresar un dato que no
	 *                                   corresponde al tipo especificado.
	 * @see InputMismatchException
	 */
	public static void main(String[] args) throws InputMismatchException {
		// Scanner recibe los datos por teclado.
		Scanner sn = new Scanner(System.in);
		// Bandera para salir del ciclo while.
		boolean salir = false;
		// Variable type int, para guardar la opci�n seleccionada.
		int opcion;
		// Se crea la m�quina expendedora
		Maquina maquina = new Maquina();
		// Se crean los articulos que tiene la m�quina expendedora
		maquina.crearArticulo("A1", "Chocolate", 10.50, 10);
		maquina.crearArticulo("A2", "Doritos", 15.50, 4);
		maquina.crearArticulo("A3", "Coca", 22.50, 2);
		maquina.crearArticulo("A4", "Gomitas", 8.75, 6);
		maquina.crearArticulo("A5", "Chips", 30, 10);
		maquina.crearArticulo("A6", "Jugo", 15, 2);
		maquina.crearArticulo("B1", "Galletas", 10, 3);
		maquina.crearArticulo("B2", "Canelitas", 120, 6);
		maquina.crearArticulo("B3", "Halls", 10.10, 10);
		maquina.crearArticulo("B4", "Tarta", 3.14, 10);
		maquina.crearArticulo("B5", "Sabritas", 15.55, 0);
		maquina.crearArticulo("B6", "Cheetos", 12.25, 10);
		maquina.crearArticulo("C1", "Rocaleta", 10, 1);
		maquina.crearArticulo("C2", "Rancherito", 14.75, 6);
		maquina.crearArticulo("C3", "Rufles", 13.15, 10);
		maquina.crearArticulo("C4", "Pizza fr�a", 22, 9);

		/**
		 * Mientras que la bandera no sea 6 (para salir), contin�a mostrando el men�.
		 */
		while (!salir) {

			System.out.println("Selecciona una opci�n");
			System.out.println("1. A�ade un producto");
			System.out.println("2. Eliminar producto");
			System.out.println("3. Modificar un producto");
			System.out.println("4. Vender un producto");
			System.out.println("5. Historial de ventas");
			System.out.println("6. Salir");
			/**
			 * Uso del try y catch para poder "atrapar" la excepci�n si no se ingresa un
			 * n�mero.
			 */
			try {
				// Guarda la opci�n seleccionada por el usuario
				opcion = sn.nextInt();
				// Scanner para datos de entrada del switch.
				Scanner sc = new Scanner(System.in);
				switch (opcion) {
				case 1:
					System.out.println("Ingresa el c�digo del producto: ");
					String codigoP = sc.nextLine();
					System.out.println("Ingresa el nombre del producto: ");
					String nombreP = sc.nextLine();
					System.out.println("Ingresa el precio del producto: ");
					double precioP = sc.nextDouble();
					System.out.println("Ingresa el stock del producto: ");
					int stock = sc.nextInt();
					maquina.crearArticulo(codigoP, nombreP, precioP, stock);
					System.out.println("------------------------------------------------------------------- \n");
					break;
				case 2:
					System.out.println("Ingresa el c�digo del producto: ");
					codigoP = sc.nextLine();
					System.out.println(maquina.removerArticulo(codigoP));
					System.out.println("------------------------------------------------------------------- \n");
					break;
				case 3:
					System.out.println("Ingresa el c�digo del producto: ");
					codigoP = sc.nextLine();
					System.out.println("�Qu� atributo quieres actualizar?  1. Nombre  2. Precio  3. Stock");
					int atributo = sc.nextInt();
					System.out.println("Ingresa el nuevo atributo / dato: ");
					sc.nextLine();
					String nuevoAtributo = sc.nextLine();
					System.out.println(maquina.actualizarArticulo(codigoP, atributo, nuevoAtributo));
					System.out.println("------------------------------------------------------------------- \n");
					break;
				case 4:
					System.out.println("Ingresa el c�digo del producto: ");
					codigoP = sc.nextLine();
					System.out.println(maquina.vender(codigoP) + "\n");
					System.out.println("------------------------------------------------------------------- \n");
					break;
				case 5:
					System.out.println(maquina.historialVentas());
					System.out.println("------------------------------------------------------------------- \n");
					break;
				case 6:
					salir = true;
					System.out.println("�Hasta luego!");
					break;
				// En caso que no se ingrese una opci�n valida del men�.
				default:
					System.out.println("Ingresa una opci�n v�lida");
				}
			} catch (InputMismatchException e) {
				System.out.println("�Error! Debes insertar un n�mero, intenta de nuevo.");

			}
		}

	}

}
