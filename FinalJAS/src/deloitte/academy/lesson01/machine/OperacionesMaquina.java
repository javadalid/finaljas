package deloitte.academy.lesson01.machine;


/**
 * Interface OperacionesMaquina, contiene la decrlaraci�n de los m�todos a ser
 * implementados por las clases que utilicen la interface.
 * 
 * @author Javier Adalid Schmid
 * @version 1.0 La primer versi�n del programa.
 * @since 10 de marzo de 2020.
 *
 */
public interface OperacionesMaquina {
	
	/**
	 * M�todo que remueve un art�culo de la lista de art�culos.
	 *
	 * @param codigoArticulo the codigo articulo
	 * @return mensaje, con el resultado de la operaci�n, type string.
	 */
	public String removerArticulo(String codigoArticulo);

	/**
	 * M�todo utilizado para modificar o actualizar alguno de los atributos del
	 * producto.
	 *
	 * @param codigoArticulo the codigo articulo
	 * @param atributoActualizar the atributo actualizar
	 * @param actualizacion the actualizacion
	 * @return mensaje, con el resultado de la operaci�n, type string.
	 */
	public String actualizarArticulo(String codigoArticulo, int atributoActualizar, String actualizacion);

	/**
	 * M�todo para realizar una venta de un producto de la m�quina expendedora.
	 *
	 * @param codigoArticulo the codigo articulo
	 * @return mensaje, con el resultado de la operaci�n, type string.
	 */
	public String vender(String codigoArticulo);

	/**
	 * M�todo que imprime todas las ventas realizadas.
	 *
	 * @return the string
	 */
	public String historialVentas();

	/**
	 * M�todo utilizado para crear un nuevo art�culo.
	 *
	 * @param codigo the codigo
	 * @param nombreProd the nombre prod
	 * @param precio the precio
	 * @param cantidad the cantidad
	 */
	public void crearArticulo(String codigo, String nombreProd, double precio, int cantidad);

}
