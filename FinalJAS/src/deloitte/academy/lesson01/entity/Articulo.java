package deloitte.academy.lesson01.entity;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase art�culo, Contiene su constructor as� como la implementaci�n de getters
 * y setters para cada uno de sus atributos.
 * 
 * @author Javier Adalid Schmid
 * @version 1.0 La primer versi�n del programa.
 * @since 10 de marzo de 2020.
 *
 */
public class Articulo {
	/** Constante LOGGER. Para generar todos los logs correspondientes */
	private static final Logger LOGGER = Logger.getLogger(Articulo.class.getName());
	// ArrayList de art�culos que guarda a todos los art�culos creados.
	public static ArrayList<Articulo> articulos = new ArrayList<Articulo>();
	private String codigo;
	private String nombreProd;
	private double precio;
	private int cantidad;
	private int vendidos;

	/**
	 * Constructor de un art�culo.
	 * 
	 * @param codigo     El c�digo del art�culo, type string
	 * @param nombreProd El nombre del producto, type string.
	 * @param precio     El precio del producto, type double.
	 * @param cantidad   La cantidad / stock del producto, type int.
	 */
	public Articulo(String codigo, String nombreProd, double precio, int cantidad) {
		super();
		// Bandera para comprobaci�n de productos con c�digo duplicado.
		boolean existe = false;
		// Se obtiene el c�digo de cada art�culo
		for (Articulo articulo : articulos) {
			// Validaci�n para evitar c�digos duplicados.
			if (articulo.getCodigo().toLowerCase().equals(codigo.toLowerCase())) {
				// Bandera en true.
				existe = true;
				break;
			}
		}
		// Si no est� duplicado lo crea y lo a�ade al ArrayList.
		if (!existe) {
			this.codigo = codigo;
			this.nombreProd = nombreProd;
			this.precio = precio;
			this.cantidad = cantidad;
			this.vendidos = 0;
			articulos.add(this);
			LOGGER.log(Level.INFO, "Articulo creado exitosamente. C�digo: " + this.codigo + "\n");
		} else {
			LOGGER.log(Level.SEVERE, "Error, se intent� crear un producto con c�digo existente. \n");
		}

	}

	/**
	 * @return El c�digo del producto
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @return El nombre del producto
	 */
	public String getNombreProd() {
		return nombreProd;
	}

	/**
	 * @param nombreProd El nombre del producto a settear
	 */
	public void setNombreProd(String nombreProd) {
		this.nombreProd = nombreProd;
	}

	/**
	 * @return El precio del producto
	 */
	public double getPrecio() {
		return precio;
	}

	/**
	 * @param precio El precio del producto a settear
	 */
	public void setPrecio(double precio) {
		this.precio = precio;
	}

	/**
	 * @return La cantidad del producto
	 */
	public int getCantidad() {
		return cantidad;
	}

	/**
	 * @param cantidad La cantidad del producto a settear.
	 */
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	/**
	 * @return vendidos La cantidad de productos vendidos.
	 */
	public int getVendidos() {
		return vendidos;
	}

	/**
	 * @param vendidos La cantidad de productos vendidosa settear.
	 */
	public void setVendidos(int vendidos) {
		this.vendidos = vendidos;
	}

}
