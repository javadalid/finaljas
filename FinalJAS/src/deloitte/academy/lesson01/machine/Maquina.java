package deloitte.academy.lesson01.machine;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import deloitte.academy.lesson01.entity.Articulo;

/**
 * Clase M�quina, implementa la interfaz OperacionesMaquina. Da cuerpo a todos
 * los m�todos de la interfaz.
 * 
 * @author Javier Adalid Schmid
 * @version 1.0 La primer versi�n del programa.
 * @since 10 de marzo de 2020. {@link deloitte.academy.lesson01.machine
 *        OperacionesMaquina}
 *
 */
public class Maquina implements OperacionesMaquina {

	/** Constante LOGGER. Para generar todos los logs correspondientes */
	private static final Logger LOGGER = Logger.getLogger(Maquina.class.getName());

	/** ArrayList de productos vendidos. */

	ArrayList<Articulo> vendidos = new ArrayList<Articulo>();

	/**
	 * Sobreescritura del m�todo de removerArticulo().
	 *
	 * @param codigoArticulo El c�digo del art�culo, type string.
	 * @return Mensaje con el resultado, type string.
	 */
	@Override
	public String removerArticulo(String codigoArticulo) {
		// Mensaje predeterminado
		String mensaje = "No se encontr� el art�culo con el c�digo: " + codigoArticulo;
		try {
			// Recorre el ArrayList de art�culos y elimina el buscado
			for (Articulo art : Articulo.articulos) {
				/*
				 * Se pasa a lower case, para que lo compruebe independientemente del uso de
				 * may�sculas / min�sculas.
				 */
				if (art.getCodigo().toLowerCase().equals(codigoArticulo.toLowerCase())) {
					Articulo.articulos.remove(art);
					mensaje = "El art�culo con c�digo: " + codigoArticulo + " ha sido eliminado.";
					break;
				}
			}
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "Error, se produjo una excepci�n. ", ex + "\n");
		}
		return mensaje;
	}

	/**
	 * Sobreescritura del m�todo actualizarArticulo().
	 *
	 * @param codigoArticulo     El c�digo del art�culo, type string.
	 * @param atributoActualizar El atributo que ser� actualizado, type int.
	 * @param actualizacion      El nuevo atributo, type string.
	 * @return Mensaje, con el resultado de la actualizaci�n, type string.
	 */
	@Override
	public String actualizarArticulo(String codigoArticulo, int atributoActualizar, String actualizacion) {

		// Mensaje predeterminado
		String mensaje = "No se encontr� el art�culo con el c�digo: " + codigoArticulo;
		try {
			// Busca el art�culo por c�digo
			for (Articulo art : Articulo.articulos) {
				if (art.getCodigo().toLowerCase().equals(codigoArticulo.toLowerCase())) {
					// Se utiliza un switch para la opci�n del atributo a modificar.
					switch (atributoActualizar) {
					// Actualiza el nombre del producto
					case 1:
						try {
							art.setNombreProd(actualizacion);
							mensaje = "El art�culo " + codigoArticulo + " , ha sido renombrado a: " + actualizacion;
						} catch (Exception ex) {
							LOGGER.log(Level.SEVERE, "Error, se produjo una excepci�n. ", ex + "\n");
						}
						break;
					// Actualiza el precio del producto.
					case 2:
						try {
							// Variable que contendr� la actualizaci�n
							double cadenaConvertida = 0;
							try {
								// Parse de string a double.
								cadenaConvertida = new Double(actualizacion);
							} catch (Exception ex) {
								LOGGER.log(Level.SEVERE, "Error en la conversi�n de String a double. ", ex + "\n");
							}
							// Validaci�n para evitar que el precio de un producto sea menor o igual a 0.
							if (cadenaConvertida > 0)
								art.setPrecio(cadenaConvertida);
							else {
								mensaje = "El precio tiene que ser mayor a 0.";
							}
						} catch (Exception ex) {
							LOGGER.log(Level.SEVERE, "Error, se produjo una excepci�n. ", ex + "\n");
						}
						break;
					case 3:
						try {
							// Variable que almacenar� la cantidad nueva
							int cadenaConvertida = 0;
							try {
								// Se realiza la conversi�n de String a Int.
								cadenaConvertida = Integer.parseInt(actualizacion);
							} catch (Exception ex) {
								LOGGER.log(Level.SEVERE, "Error en la conversi�n de String a Int. ", ex + "\n");
							}
							// Validaci�n para evitar que el precio de un producto sea menor a 0.
							if (cadenaConvertida >= 0)
								art.setCantidad(cadenaConvertida);
							else {
								mensaje = "La cantidad tiene que ser mayor o igual a 0.";
							}
						} catch (Exception ex) {
							LOGGER.log(Level.SEVERE, "Error, se produjo una excepci�n. ", ex + "\n");
						}
						break;
					// Mensaje si se elige una opci�n diferente a 1,2 o 3.
					default:
						mensaje = "El atributo a actualizar s�lo puede ser 1, 2, o 3.";
						break;
					}
					break;
				}
			}
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "Error, se produjo una excepci�n. ", ex + "\n");
		}
		// Devuelve el mensaje.
		return mensaje;
	}

	/**
	 * Sobreescritura del m�todo de vender().
	 *
	 * @param codigoArticulo El c�digo del art�culo, type string.
	 * @return Mensaje, con el resultado de la venta, type string.
	 */
	@Override
	public String vender(String codigoArticulo) {
		// Mensaje predeterminado.
		String mensaje = "No se encontr� el art�culo con el c�digo: " + codigoArticulo;
		try {
			// Bandera para saber si fue encontrado
			boolean found = false;
			// Comprobaci�n del c�digo del art�culo
			for (Articulo art : Articulo.articulos) {
				if (art.getCodigo().toLowerCase().equals(codigoArticulo.toLowerCase())) {
					// Cambia la bandera a true.
					found = true;
					// Verificar stock antes de la venta
					if (art.getCantidad() >= 1) {
						art.setCantidad(art.getCantidad() - 1);
						art.setVendidos(art.getVendidos() + 1);
						mensaje = "�Venta exitosa! Total: $" + art.getPrecio();
						// Lo a�ade a la lista solo si no est�, para evitar duplicados.
						if (!vendidos.contains(art))
							vendidos.add(art);
						break;
					} else {
						mensaje = "No hay suficiente stock del producto con c�digo " + codigoArticulo
								+ " para esta venta";
						break;
					}

				}
			}
			// Si no se encontr� el c�digo.
			if (!found) {
				mensaje = "El art�culo con c�digo " + codigoArticulo + " no fue encontrado.";
			}
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "Error, se produjo una excepci�n. ", ex + "\n");
		}
		// Devuelve el mensaje.
		return mensaje;
	}

	/**
	 * Sobreescritura del m�todo del historialVentas().
	 *
	 * @return ventas, el ArrayList de vendidos, como una cadena de texto con
	 *         concatenaciones, type string.
	 */
	@Override
	public String historialVentas() {
		// Cadena a la que se le concatenan los resultados.
		String ventas = "";
		try {
			// Recorre el ArrayList de los vendidos.
			for (Articulo art : vendidos) {
				// Concatenaci�n a ventas.
				ventas += art.getNombreProd() + " --- Vendidos: " + art.getVendidos() + " --- Total: $"
						+ art.getVendidos() * art.getPrecio() + "\n";
			}
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "Error, se produjo una excepci�n. ", ex + "\n");
		}
		// Devuelve ventas como string.
		return ventas;
	}

	/**
	 * Sobreescritura del m�todo de crearArticulo(). Un "bridge" al constructor de
	 * producto para acceder a �l a trav�s de la interfaz.
	 *
	 * @param codigo     El c�digo del art�culo, type string.
	 * @param nombreProd El nombre del producto, type string.
	 * @param precio     El precio del producto, type double.
	 * @param cantidad   La cantidad / stock del producto, type int.
	 */
	@Override
	public void crearArticulo(String codigo, String nombreProd, double precio, int cantidad) {
		try {
			// Instancia de art�culo.
			new Articulo(codigo, nombreProd, precio, cantidad);
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "Error, se produjo una excepci�n. ", ex + "\n");
		}
	}
}
